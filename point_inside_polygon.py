#!/usr/bin/python
'''
File Name : point_inside_polygon.py
Creation Date : 07-11-2015
Last Modified : Fri 09 Feb 2018 12:08:55 AEDT
Author : Ruth Lorenz (r.lorenz@unsw.edu.au)
Purpose : function to determine if a point is inside a polygon
	for regional analysis e.g. SREX regions
        lon: longitudes, from -180 to 180 
        lat: latitudes
        border: lat, lon pairs that determine border of the area to be checked
                [(lon_min, lat_max), (lon_max, lat_max), (lon_max, lat_min),
                 (lon_min, lat_min))]
'''
import sys

def point_inside_polygon(lat, lon, border):
    n = len(border)
    inside = False
    #p1lat, p1lon = border[0]
    #p2lat, p2lon = border[1]
    p1lon, p1lat = border[0]
    p2lon, p2lat = border[2]
    # check if area crosses 0 meridian, in that case shift lons
    if (p1lon < 0.0) & (p2lon > 0.0):
        #print "p1lon= "+str(p1lon)
        meridian = True
        print 'mask crossing 0 meridian'
        shift = abs(p1lon)
        lon = lon + shift
        p1lon = p1lon + shift
        #print "p1lon= "+str(p1lon)
    else:
        meridian = False
    # check if longitudes from -180 to 180, if not and crossing 0 meridian exit
    if (meridian == True):
        if ((lon - shift) > 180.0 ):
            print 'Warning: longitudes need to be from -180 to 180'
            print 'otherwise problem when crossing 0 meridian,'
            print 'this is the case here, exiting'
            sys.exit
    for i in range(n + 1):
        p2lon, p2lat = border[i % n]
        if meridian:
            p2lon = p2lon + shift
    #        print "p2lon= "+str(p2lon)
        if lon > min(p1lon, p2lon):
            if lon <= max(p1lon, p2lon):
                if lat <= max(p1lat, p2lat):
                    if p1lon != p2lon:
                        xinters = ((lon - p1lon) * (p2lat - p1lat) / 
                                   (p2lon - p1lon) + p1lat)
                        if p1lat == p2lat or lat <= xinters:
                            inside = not inside
        p1lat, p1lon = p2lat, p2lon

    return inside
