#!/usr/bin/python
'''
File Name : func_write_netcdf.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 12-05-2016
Modified: Thu 12 May 2016 04:03:39 PM CEST
Purpose: function to write data into netcdf

'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
import datetime as dt

def func_write_netcdf(outfile, data, variable, lonvar, latvar, timevar = None,
                      time_unit = None, time_cal = None, Longname = None,
                      var_units = None, Description = None, comment = None,
                      script = None, archive = None):
    fout = nc.Dataset(outfile, mode = 'w')
    fout.createDimension('lat', latvar.shape[0])
    fout.createDimension('lon', lonvar.shape[0])
    if isinstance (timevar, (int, long, float, np.ndarray, list)):
        fout.createDimension('time', len(timevar))
        timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
        if isinstance (time_unit, basestring):
            setattr(timeout, 'units', str(time_unit))
        else:
            setattr(timeout, 'units', 'days since %s' %(
                (timevar[0].year, 1, 1, 0), '%Y-%m-%d %H:%M:%S'))
        if isinstance (time_cal, basestring):
            setattr(timeout, 'calendar', str(time_cal))

    latout = fout.createVariable('lat', 'f8', ('lat'), fill_value = 1e20)
    setattr(latout, 'Longname', 'Latitude')
    setattr(latout, 'units', 'degrees_north')

    lonout = fout.createVariable('lon', 'f8', ('lon'), fill_value = 1e20)
    setattr(lonout, 'Longname', 'Longitude')
    setattr(lonout, 'units', 'degrees_east')

    if isinstance (timevar, (int, long, float, np.ndarray, list)):
        dataout = fout.createVariable(variable, 'f8', ('time', 'lat', 'lon'),
                                      fill_value = 1e20)
    else:
        dataout = fout.createVariable(variable, 'f8', ('lat','lon'),
                                      fill_value = 1e20)
    if isinstance (Longname, basestring): setattr(dataout, 'Longname', Longname)
    if isinstance (var_units, basestring): setattr(dataout, 'units',
                                                   str(var_units))
    if isinstance (Description, basestring): setattr(dataout, 'description',
                                                     Description)

    if isinstance (timevar, (int, long, float, np.ndarray)):
        timeout[:] = timevar[:]
    latout[:] = latvar[:]
    lonout[:] = lonvar[:]
    dataout[:] = data[:]

    # Set global attributes
    setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
    setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
    if isinstance (comment,basestring): setattr(fout, "comment", comment)

    if isinstance (script, basestring): setattr(fout, "Script", script)
    if isinstance (archive, basestring): setattr(fout,
                                         "Input files located in:", archive)
    fout.close()
 
def func_write_ts_netcdf(outfile, data, variable, timevar,
                         time_unit = None, time_cal = None, Longname = None,
                         var_units = None, Description = None, comment = None,
                         script = None, archive = None):
    fout = nc.Dataset(outfile, mode = 'w')

    fout.createDimension('time', len(timevar))
    timeout = fout.createVariable('time', 'f8', ('time'), fill_value = 1e20)
    if isinstance (time_unit, basestring):
        setattr(timeout, 'units', str(time_unit))
    else:
        setattr(timeout, 'units', 'days since %s' %((timevar[0].year,
                                                     1, 1, 0),
                                                    '%Y-%m-%d %H:%M:%S'))
    if isinstance (time_cal, basestring):
        setattr(timeout, 'calendar', str(time_cal))

    dataout = fout.createVariable(variable, 'f8', ('time'),
                                  fill_value = 1e20)

    if isinstance (Longname, basestring): setattr(dataout, 'Longname', Longname)
    if isinstance (var_units, basestring): setattr(dataout, 'units',
                                                   str(var_units))
    if isinstance (Description, basestring): setattr(dataout, 'description',
                                                     Description)

    timeout[:] = timevar[:]
    dataout[:] = data[:]

    # Set global attributes
    setattr(fout, "author", "Ruth Lorenz @IAC, ETH Zurich, Switzerland")
    setattr(fout, "contact", "ruth.lorenz@env.ethz.ch")
    setattr(fout, "creation date", dt.datetime.today().strftime('%Y-%m-%d'))
    if isinstance (comment,basestring): setattr(fout, "comment", comment)

    if isinstance (script, basestring): setattr(fout, "Script", script)
    if isinstance (archive, basestring): setattr(fout,
                                         "Input files located in:", archive)
    fout.close()
