#!/usr/bin/python
'''
File Name: calc_RMSE_obs_mod_3D.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 02-03-2016
Modified: Mon 04 Jul 2016 06:55:10 PM CEST
Purpose: Function that calculates RMSE between two 3D datasets,
         e.g. model (x) versus reference (ref),
         needs [time, lat, lon] data, time should be monthly data from which
         rmse can be calculated for months, seasons or annual time scales.
         time can be size 0 if loop over months, seasons is outside of function

'''
import numpy as np
from math import atan
import sys
import logging
from info_utils import set_logger
logger = logging.getLogger(__name__)
set_logger(level = logging.INFO)
# for py3 forward compatibility
from builtins import range

def rmse_3D(ref, x, lat, lon, tp = None, w_t = 1):
    rad = 4.0 * atan(1.0) / 180
    w_lat = np.cos(lat * rad)
    W = 0
    dims = x.shape
    if (len(dims) == 3):
        ntime = x.shape[0]
        if (x.shape[1] != len (lat)) | (x.shape[2] != len (lon)):
            logger.error('Error in dimensions: needs to be [time, lat, lon], exiting')
            sys.exit
        tmp = np.ndarray((ntime, len(lat), len(lon)))
        if (tp == "cyc") & (ntime == 12): #annual data
            for i in range(len(lon)):
                for j in range(len(lat)):
                    for M in range(0, ntime):
                        #weight depending on length of month
                        if ((M == 0) or (M == 2) or (M == 4) or (M == 6) or
                            (M == 7) or (M == 9) or (M == 11)):
                            w_t = 31
                        elif (M == 1):
                            w_t = 28
                        else:
                            w_t = 30
                        tmp[M, j, i] = w_lat[j] * w_t * (x[M, j, i] -
                                                         ref[M, j, i]) ** 2
                        if not ((np.isnan(x[M, j, i])) or
                                (np.isnan(ref[M, j, i]))):
                            W = W + w_lat[j] * w_t
            rmse = np.sqrt(np.nansum(tmp) / W)
        elif (tp == "cyc") & (ntime != 12):
            logger.error("Error: annual cycle data needs to be of length 12, exiting")
            sys.exit
        elif (ntime == 1): #trend or seasonal, monthly data with loop over time outside function
            for i in range(len(lon)):
                for j in range(len(lat)):
                    tmp[0, j, i] = w_lat[j] * (x[0, j, i] - ref[0, j, i]) ** 2
                    if not ((np.isnan(x[0, j, i])) or
                            (np.isnan(ref[0, j, i]))):
                        W = W + w_lat[j]
            rmse = np.sqrt(np.nansum(tmp) / W)
        else:
            rmse = np.ndarray((ntime))
            for M in range(0, ntime):
                for i in range(len(lon)):
                    for j in range(len(lat)):
                        tmp[M, j, i] = w_lat[j] * (x[M, j, i] -
                                                   ref[M, j, i]) ** 2
                        if not ((np.isnan(x[M, j, i])) or
                                (np.isnan(ref[M, j, i]))):
                            W = W + w_lat[j]
                rmse = np.sqrt(np.nansum(tmp) / W)
    elif (len(dims) == 2):
        tmp = np.ndarray((len(lat), len(lon)))
        for i in range(len(lon)):
            for j in range(len(lat)):
                tmp[j, i] = w_lat[j] * (x[j, i] - ref[j, i]) ** 2
                if not ((np.isnan(x[j, i])) or
                        (np.isnan(ref[j, i]))):
                    W = W + w_lat[j]
        rmse = np.sqrt(np.nansum(tmp) / W)
    return rmse
    
