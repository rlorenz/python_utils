#!/usr/bin/python
'''
File Name : stations_utils.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 20-04-2018
Modified: Fri 20 Apr 2018 04:33:01 PM CEST
Purpose: utility functions to handle station data


'''
# convert degrees, minutes, seconds to lat lon
def dms2dd(degrees, minutes, seconds, direction):
    dd = float(degrees) + float(minutes) / 60 + float(seconds) / (60 * 60)
    if direction == '-':
        dd *= -1
    return dd

