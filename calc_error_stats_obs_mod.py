#!/usr/bin/python
'''
File Name : calc_MBE_obs_mod.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 19-12-2017
Modified: Tue 19 Dec 2017 05:21:10 PM CET
Purpose: calculate mean bias error (MBE) between two datasets,
         e.g. model (x) versus reference (ref).
         

'''
import numpy as np
import math
import sys

# Mean bias error
def mbe_1D(ref, x):
    dims_x = x.shape
    dims_ref = ref.shape
    if (dims_x != dims_ref):
        print 'Error: reference and model do not have same dimensions, exiting.'
        sys.exit
    if (len(dims_x) > 1):
        print 'Error: Need 1D data (timeseries) only, exiting.'
        sys.exit
    mbe = np.nanmean(x - ref)
    return mbe

def mbe_3D(ref, x):
    dims_x = x.shape
    dims_ref = ref.shape
    if (dims_x != dims_ref):
        print 'Error: reference and model do not have same dimensions, exiting.'
        sys.exit
    if (len(dims_x) != 3):
        print 'Error in dimensions: need 3D data (time, lat, lon), dimension is %s, exiting.' %(len(dims_x))
        sys.exit
    for i in range(len(lon)):
        for j in range(len(lat)):
            error[j, i] = np.nanmean(x[j, i] - ref[j, i])
    mbe = np.nansum(error)
    return mbe

# standard deviation
def sd_1D(ref, x):
    dims_x = x.shape
    dims_ref = ref.shape
    if (dims_x != dims_ref):
        print 'Error: reference and model do not have same dimensions, exiting.'
        sys.exit
    if (len(dims_x) > 1):
        print 'Error: Need 1D data (timeseries) only, exiting.'
        sys.exit
    x_mean = np.nanmean(x)
    ref_mean = np.nanmean(ref)
    pow_x = np.empty(dims_x)
    pow_ref = np.empty(dims_ref)
    for t in xrange(len(x)):
        pow_x[t] = math.pow((x[t] - x_mean), 2)
        pow_ref[t] = math.pow((ref[t] - ref_mean), 2)
    sd = abs(1 - (np.sqrt(np.nansum(pow_x) / (len(x) - 1)) / 
             np.sqrt(np.nansum(pow_ref) / (len(x) - 1))))
    return sd

# correlation
def corr_1D(ref, x):
    dims_x = x.shape
    dims_ref = ref.shape
    if (dims_x != dims_ref):
        print 'Error: reference and model do not have same dimensions, exiting.'
        sys.exit
    if (len(dims_x) > 1):
        print 'Error: Need 1D data (timeseries) only, exiting.'
        sys.exit
    n = len(x)
    pow_x = np.empty(dims_x)
    pow_ref = np.empty(dims_ref)
    for t in xrange(len(x)):
        pow_x[t] = math.pow(x[t], 2)
        pow_ref[t] = math.pow(ref[t], 2)
    corr = ((n * np.nansum(ref * x) - (np.nansum(ref) * np.nansum(x))) /
            np.sqrt((n * np.nansum(pow_ref) - 
                     math.pow(np.nansum(ref), 2)) * (n * np.nansum(pow_x) - 
                                                     math.pow(np.nansum(x),
                                                              2))))
    return corr

# normalized mean error
def nme_1D(ref, x):
    dims_x = x.shape
    dims_ref = ref.shape
    if (dims_x != dims_ref):
        print 'Error: reference and model do not have same dimensions, exiting.'
        sys.exit
    if (len(dims_x) > 1):
        print 'Error: Need 1D data (timeseries) only, exiting.'
        sys.exit
    norm = np.empty(dims_ref)
    for t in xrange(len(ref)):
        norm[t] = abs(np.nanmean(ref) - ref[t])
    nme = np.nansum(abs(x - ref)) / np.nansum(norm)
    return nme
