#!/usr/bin/python
'''
File Name : rank_prob_score.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 11-01-2018
Modified: Thu 11 Jan 2018 09:20:30 AM CET
Purpose: calculate the ranked probability score
'''
import numpy as np
from scipy.stats import norm

def rank_prob_score(pred, obs):
    """pred: array of predictions, [predictions, categories] 
       obs: array of observations, [observations, categories] 
    """
    npred = pred.shape[0]
    ncat = pred.shape[1]

    rps = np.empty((npred), dtype = float)

    for rr in xrange(npred):
        pred_cdf = np.empty((ncat), dtype = float)
        obs_cdf = np.empty((ncat), dtype = float)
        for i in xrange(ncat):
            if i == 0:
                pred_cdf[i] = pred[rr, i]
                obs_cdf[i] = obs[rr, i]
            else:
                pred_cdf[i] = pred_cdf[i - 1] + pred[rr, i]
                obs_cdf[i] = obs_cdf[i - 1] + obs[rr, i]
        cumulative = np.sum((pred_cdf - obs_cdf) ** 2)
        rps[rr] = (1. / (ncat - 1.)) * cumulative
    return(rps)
