#!/usr/bin/python
'''
File Name : info_utils.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 17-05-2018
Modified: Thu 17 May 2018 12:26:07 PM CEST
Purpose: Utilities for code info, logging etc.


'''
import logging
def set_logger(
        format = '%(asctime)s - %(name)s: %(module)s - %(message)s',
        level = logging.INFO,
        filename = None,
        filemode = 'w'):
    """Set up a basic logger"""
    logging.basicConfig(**locals())

