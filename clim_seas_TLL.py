#!/usr/bin/python
'''
File Name : clim_seas_TLL.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-03-2016
Modified: Fri 11 Nov 2016 11:35:53 AM CET
Purpose: functions to calculate climatologies, seasonal means etc.
	 for [time, lat, lon] data 
'''
import numpy as np

# calculate seasonal averages for each year from monthly values
def clim_seas_mon_TLL(x, months, years):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    print dims
    nyears = years[ - 1] - years[0] + 1
    seas = np.ndarray((4, nyears, dims[1], dims[2]))
    if (months[0] == 1):
        print "Timeseries starts in January, DJF seasonal mean will include" \
              "JF from first year and D in last year, if this is not intended" \
              "provide timeseries starting in December, ending in November"
    for s in xrange(1, 5):
        S = s - 1
        Y = 0
        for y in xrange(years[0], years[ - 1] + 1):
            if (s == 1):
                x_tmp = x[np.where(((years == y) & (months == 1)) |
                                   ((years == y) & (months == 2)) |
                                   ((years == y - 1) & (months == 12))), :, :]
            elif (s == 2):
                x_tmp = x[np.where(((years == y) & (months == 3)) |
                                   ((years == y) & (months == 4)) |
                                   ((years == y) & (months == 5))), :, :]
            elif (s == 3):
                x_tmp = x[np.where(((years == y) & (months == 6)) |
                                   ((years == y) & (months == 7)) |
                                   ((years == y) & (months == 8))), :, :]
            else:
                x_tmp = x[np.where(((years == y) & (months == 9)) |
                                   ((years == y) & (months == 10)) |
                                   ((years == y) & (months == 11))), :, :]
	    tmp = np.mean(np.squeeze(x_tmp), axis = 0)
            seas[S, Y, :, :] = tmp
            Y = Y + 1
    return seas

# calculate monthly climatology
def clim_mon_TLL(x, months):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    clim = np.ndarray((12, dims[1], dims[2]))
    for m in xrange(1, 13):
        M = m - 1
        clim[M, :, :] = np.nanmean(x[months == m, :, :], axis = 0)
    return clim

# calculate monthly variability
def std_mon_TLL(x, months):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    std = np.ndarray((12, dims[1], dims[2]))
    for m in xrange(1, 13):
        M = m - 1
        std[M, :, :] = np.nanstd(x[months == m, :, :], axis = 0)

    return std

# calculate seasonal means from monthly data
def seas_avg_mon_TLL(x, months):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    seas = np.ndarray((4, dims[1], dims[2]))
    if (months[0] == 1):
        print "Timeseries starts in January, DJF seasonal mean will include" \
              "JF from first year and D in last year, if this is not intended"\
              "provide timeseries starting in December, ending in November"
    for s in xrange(1, 5):
        S = s - 1
        if (s == 1):
            seas[S, :, :] = np.nanmean(x[np.where((months == 1) | (months == 2)
                                                  | (months == 12)), :, :], 
                                       axis = 1).squeeze()
        elif (s == 2):
            seas[S, :, :] = np.nanmean(x[np.where((months == 3) | (months == 4)
                                                  | (months == 5)), :, :],
                                       axis = 1).squeeze()
        elif (s == 3):
            seas[S, :, :] = np.nanmean(x[np.where((months == 6) | (months == 7)
                                                  | (months == 8)), :, :],
                                       axis = 1).squeeze()
        else:
            seas[S, :, :] = np.nanmean(x[np.where((months == 9) | (months == 10)
                                                  | (months == 11)), :, :],
                                       axis = 1).squeeze()
    return seas

# calculate seasonal variability from monthly values
def seas_std_mon_TLL(x, months, years):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    seas_std = np.ndarray((4, dims[1], dims[2]))
    if (months[0] == 1):
        print "Timeseries starts in January, DJF seasonal std will include JF "\
              "from first year and D in last year, if this is not intended" \
              "provide timeseries starting in December, ending in November"
    seas_avg_temp = clim_seas_mon_TLL(x, months, years)
    for S in xrange(0, 4):
        seas_std[S, :, :] = np.nanstd(seas_avg_temp[S, :, :, :],
                                          axis = 0).squeeze()
    return seas_std

# calculate annual variability from monthly values
def ann_std_mon_TLL(x, years):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    nyears = dims[0] / 12
    mean = np.ndarray((nyears, dims[1], dims[2]))
    A = 0
    for a in xrange(0, len(years), 12):
        yr = years[a]
	mean[A, :, :] = np.nanmean(x[np.where(years == yr), :, :],
                                 axis = 1).squeeze()
        A = A + 1
    ann = np.nanstd(mean, axis = 0)
    return ann

# calculate annual averages from monthly values
def ann_avg_mon_TLL(x, years):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    nyears = dims[0] / 12
    ann = np.ndarray((nyears, dims[1], dims[2]))
    A = 0
    for a in xrange(0, len(years), 12):
        yr = years[a]
        ann[A, :, :] = np.nanmean(x[np.where(years == yr), :, :],
                                 axis = 1).squeeze()
        A = A + 1
    return ann

# calculate seasonal averages for each year from monthly values
# give month names in season
def seas_mon_TLL(x, months, years, season):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    print dims
    nyears = years[- 1] - years[0] + 1
    seas = np.ndarray((nyears, dims[1], dims[2]))

    Y = 0
    for y in xrange(years[0], years[- 1] + 1):
        if (season == 'DJF'):
            x_tmp = x[np.where(((years == y) & (months == 1)) |
                               ((years == y) & (months == 2)) |
                               ((years == y - 1) & (months == 12))), :, :]
        elif (season == 'MAM'):
            x_tmp = x[np.where(((years == y) & (months == 3)) |
                               ((years == y) & (months == 4)) |
                               ((years == y) & (months == 5))), :, :]
        elif (season == 'JJA'):
            x_tmp = x[np.where(((years == y) & (months == 6)) |
                               ((years == y) & (months == 7)) |
                               ((years == y) & (months == 8))), :, :]
        elif (season == 'SON'):
            x_tmp = x[np.where(((years == y) & (months == 9)) |
                               ((years == y) & (months == 10)) |
                               ((years == y) & (months == 11))), :, :]
        elif (season == 'JF'):
            x_tmp = x[np.where(((years == y) & (months == 1)) |
                               ((years == y) & (months == 2))), :, :]
        elif (season == 'FM'):
            x_tmp = x[np.where(((years == y) & (months == 2)) |
                               ((years == y) & (months == 3))), :, :]
        elif (season == 'MA'):
            x_tmp = x[np.where(((years == y) & (months == 3)) |
                               ((years == y) & (months == 4))), :, :]
        elif (season == 'AM'):
            x_tmp = x[np.where(((years == y) & (months == 4)) |
                               ((years == y) & (months == 5))), :, :]
        elif (season == 'MJ'):
            x_tmp = x[np.where(((years == y) & (months == 5)) |
                               ((years == y) & (months == 6))), :, :]
        elif (season == 'JJ'):
            x_tmp = x[np.where(((years == y) & (months == 6)) |
                               ((years == y) & (months == 7))), :, :]
        elif (season == 'JA'):
            x_tmp = x[np.where(((years == y) & (months == 7)) |
                               ((years == y) & (months == 8))), :, :]
        elif (season == 'AS'):
            x_tmp = x[np.where(((years == y) & (months == 8)) |
                               ((years == y) & (months == 9))), :, :]
        elif (season == 'SO'):
            x_tmp = x[np.where(((years == y) & (months == 9)) |
                               ((years == y) & (months == 10))), :, :]
        elif (season == 'ON'):
            x_tmp = x[np.where(((years == y) & (months == 10)) |
                               ((years == y) & (months == 11))), :, :]
        elif (season == 'ND'):
            x_tmp = x[np.where(((years == y) & (months == 11)) |
                               ((years == y - 1) & (months == 12))), :, :]
        elif (season == 'DJFMAM'):
            x_tmp = x[np.where(((years == y) & (months == 1)) |
                               ((years == y) & (months == 2)) |
                               ((years == y) & (months == 3)) |
                               ((years == y) & (months == 4)) |
                               ((years == y) & (months == 5)) |
                               ((years == y - 1) & (months == 12))), :, :]
        elif (season == 'JJASON'):
            x_tmp = x[np.where(((years == y) & (months == 6)) |
                               ((years == y) & (months == 7)) |
                               ((years == y) & (months == 8)) |
                               ((years == y) & (months == 9)) |
                               ((years == y) & (months == 10)) |
                               ((years == y) & (months == 11))), :, :]
        else:
            print'%s is not a known season, exiting' %season
            sys.exit

        tmp = np.mean(np.squeeze(x_tmp), axis = 0)
        seas[Y, :, :] = tmp
        Y = Y + 1
    return seas
