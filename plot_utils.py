#!/usr/bin/python
'''
File Name : plot_utils.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 01-02-2019
Modified: Fri 01 Feb 2019 09:56:06 AM CET
Purpose: Utilities to plot maps etc. using cartopy


'''
import os
import numpy as np
import xarray as xr

import cartopy.util
import cartopy.crs as ccrs
import matplotlib.pyplot as plt

def plot_map_stip_hatch(data_array, title=None, subpanel=None,
                        method='contourf',
                        vmin=None, vmax=None, colors='YlOrRd', levels=15,
                        R=None, sig=None, masko=None, region=None)
    # add cyclic data
    data, lons = cartopy.util.add_cyclic_point(
        data_array, coord=data_array['lon'])
    lats = data_array['lat']
    if R:
        R_cyc = cartopy.util.add_cyclic_point(R)
    if sig:
        sig_cyc = cartopy.util.add_cyclic_point(sig)

    # mask data in case mask provided TO DO
    #if masko:
        


    # plot data
    fig = plt.figure(figsize=(8,6))
    ax = plt.axes(projection=ccrs.Robinson(central_longitude=10))
    ax.contourf(lons, lats, data, transform=ccrs.PlateCarree())

    lev_hatch = [sig]
    hatch = sig.plot.contourf(ax=ax, transform=ccrs.PlateCarree(),
                              levels = lev_hatch, hatches = ["", "///"],
                              alpha=0)
    if R:
        lev_stipp1 = [R.min(), 0.8, R.max()]
        stipp1 = ax.contourf(lons, lats, R_cyc, transform=ccrs.PlateCarree(),
                             levels = lev_stipp1, hatches = ["", ".."], alpha=0)
        lev_stipp2 = [R.min(), 0.95, R.max()]
        stipp2 = ax.contourf(lons, lats, R_cyc, transform=ccrs.PlateCarree(),
                             levels = lev_stipp2, hatches = ["", "**"], alpha=0)

    if sig:
        # hatch where >=80% of models show no significant change
        nr_models80 = len(sig['model_ensemble']) * 0.8
        thres = xr.where(sig.sum(dim='model_ensemble') < nr_models80, 1, 0)
        hatch = ax.contourf(lons, lats, thres.data,
                            transform=ccrs.PlateCarree(),
                            levels = [-0.5, 0.5, 2], hatches = ["", "//"],
                            alpha=0)
    ax.coastlines()

    plt.show()
