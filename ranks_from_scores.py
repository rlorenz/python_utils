#!/usr/bin/python
'''
File Name: ranks_from_scores.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 15-12-2017
Modified: Fri 15 Dec 2017 03:00:30 PM CET
Purpose:


'''
def ranks_from_scores(sorted_scores):
    """sorted_scores: a list of tuples (object_id, score),
       return a mapping of object IDs to ranks
    """
    ranks = {}
    previous_score = object()
    for index, (obj_id, score) in enumerate(sorted_scores):
        if score != previous_score:
            previous_score = score
            rank = index + 1
        ranks[obj_id] = rank
    return ranks
