#!/usr/bin/python
'''
File Name : tas_region_test.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 03-03-2016
Modified: Thu 03 Mar 2016 03:42:23 PM CET
Purpose: test function point_inside_polygon


'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt
import matplotlib.pyplot as plt
import sys
import os # operating system interface
from os.path import expanduser
home = expanduser("~") # Get users home directory
sys.path.insert(0,home+'/scripts/plot_scripts/utils/')
from draw_utils import draw
from mpl_toolkits.basemap import shiftgrid
from point_inside_polygon import point_inside_polygon
###
# Define input
###
variable = 'T2M'

path = '/net/tropo/climphys/rlorenz/Datasets/ERAint/ERAint_mon/'
outdir = '/net/tropo/climphys/rlorenz/ERAint/%s/' %(variable)
syear = 1979
eyear = 2005
nyears = eyear - syear + 1

grid = 'g025'

region = "Europe"
latmin = 30
latmax = 70
lonmin = -10
lonmax = 50

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)

#read obs data
print "Read ERAint data"

ifile = nc.Dataset(path+'T2M_monthly_ERAint_197901-201412_'+grid+'.nc')
obslon = ifile.variables['lon'][:]
obslat = ifile.variables['lat'][:]
obstime = ifile.variables['time']

print "Create dates from netcdf time for ERAint"
obstime_units = obstime.units
obstime_cal = obstime.calendar
obscdftime = utime(obstime_units,calendar=obstime_cal)
obsdates=obscdftime.num2date(obstime[:])
obsyears=np.asarray([obsdates[i].year for i in xrange(len(obsdates))])
obsdates=obsdates[(obsyears>=syear) & (obsyears<=eyear)]
obsmonths = np.asarray([obsdates[i].month for i in xrange(len(obsdates))])

temp = ifile.variables['T2M'][(obsyears>=syear) & (obsyears<=eyear),:,:] #ERAint data is in degrees Celsius -> convert to Kelvin
temp_obs = temp + 273.15

temp_obs_clim = np.ndarray((12,len(obslat),len(obslon)))
#calculate climatology
print "Calculate obs climatology"
for m in range(1,13):
    M = m-1
    temp_obs_clim[M,:,:] = np.mean(temp_obs[obsmonths==m,:,:],axis = 0)
ifile.close()

#mean over time
temp_obs_avg = np.mean(temp_obs,axis = 0)

#lons need to be -180 tp 180 for point_inside_polygon
#check and shift if necessary
if (obslon[0] >= 0) & (obslon[-1] > 180):
        obslon_orig = obslon[:]
        temp_obs_avg,obslon =  shiftgrid(180., temp_obs_avg, obslon,start=False)

temp_obs_avg_reg = np.ndarray((len(obslat),len(obslon)))
temp_obs_clim_reg = np.ndarray((12,len(obslat),len(obslon)))
for ilat in range(len(obslat)):
    for ilon in range(len(obslon)):
        #print obslat[ilat], obslon[ilon]
        if (point_inside_polygon(obslat[ilat],obslon[ilon],[(latmax,lonmin),(latmax,lonmax),(latmin,lonmax),(latmin,lonmin)])):
            temp_obs_avg_reg[ilat,ilon] = temp_obs_avg[ilat,ilon]
            temp_obs_clim_reg[:,ilat,ilon] = temp_obs_clim[:,ilat,ilon]
        else:
            temp_obs_avg_reg[ilat,ilon] = np.NaN
            temp_obs_clim_reg[:,ilat,ilon] = np.NaN

lev = np.arange(271,303,3)
draw(temp_obs_avg_reg,obslat, obslon, title=variable, levels = lev)
plt.savefig(outdir+variable+'_'+region+'_test_inside_poly.pdf')

#regional mean
temp_obs_clim_reg_avg = np.apply_over_axes(np.nanmean, temp_obs_clim_reg, (1, 2))
fig = plt.figure(figsize=(10, 5),dpi=300)
plt.plot(range(12),temp_obs_clim_reg_avg[:,0,0],'k-',label="ERAint",linewidth=3.0)
plt.xlabel('Month')
plt.ylabel(region+' mean temperature [K]') #can use LaTeX for subscripts etc
plt.grid(True)
leg=plt.legend(loc='upper left') #leg defines legend -> can be modified
leg.draw_frame(False)

plt.savefig(outdir+variable+'_'+region+'_mean.pdf')
