#!/usr/bin/python
'''
File Name: get_area.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-02-2018
Modified: Wed 07 Feb 2018 03:14:14 PM CET
Purpose: calculate area of a gridcell for the atmospheric file
         in meters^2
'''
import numpy as np
import math

def get_atm_area(lat, lon):
    jlat = len(lat)
    ilon = len(lon)


    rad = 4.0 * math.atan(1.0) / 180.0
    re = 6371220.0
    rr = re * rad

    dlon = abs(lon[2] - lon[1]) * rr
    dx = dlon * np.cos(lat * rad)

    dy = np.zeros((jlat), dtype = type(dx))

    dy[0] = abs(lat[2] - lat[1]) * rr
    dy[1 : jlat - 1] = abs(lat[2 : jlat] - lat[1 : jlat - 1]) * rr
    dy[jlat - 1] = abs(lat[jlat - 1] - lat[jlat - 2]) * rr

    dx_reshape = dx.reshape(jlat, 1)
    dy_reshape = dy.reshape(jlat, 1)

    area_array = np.zeros((jlat, ilon), dtype = type(dx))

    tarea = dx_reshape * dy_reshape + area_array

    return tarea

