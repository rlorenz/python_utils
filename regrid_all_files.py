#!/usr/bin/python
'''
File Name : regrid_all_files.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 30-06-2016
Modified: Thu 30 Jun 2016 04:38:12 PM CEST
Purpose: regrid all files in folder or found matching pattern


'''
#import sys
import os # operating system interface
import glob

###
# Define input
###
archive = '/net/tropo/climphys/rlorenz/CMIP5_HW/TX90_HW/bp_1961-1990/historical_CMIP5_seasons/'
#archive = '/net/tropo/climphys/rlorenz/processed_CMIP5_data/corrETT/TX90_HW/rcp85_CMIP5_daily_p1/'
outdir = archive
regrid_func = 'remapcon2'
grid = '/home/rlorenz/scripts/postproc_eraint/25x25grid' #'r144x72'
GRID = 'g025'

#ifile = 'LandCoupPi_*_day_*_historical_*_19510101-20051231.nc'
ifile = 'tx90pct_heatwaves_*_yearly_*.nc'
#force to update no matter if file alrady exists?
update = False

if (os.access(outdir,os.F_OK)==False):
        os.makedirs(outdir)
 
for filename in glob.glob(archive+ifile):
    print "Regrid "+filename+" data"
    tmp_name = filename.split('.nc')[0]
    outfile = '%s_%s_%s.nc' %(tmp_name,regrid_func,GRID)
    if (os.path.isfile(outfile)) and not update:
        continue
    else:
            os.system('cdo %s,%s %s %s' %(regrid_func,grid,filename,outfile))
