#!/usr/bin/python
'''
File Name : plot_map_from_netcdf.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 12-10-2017
Modified: Thu 12 Oct 2017 10:44:19 AM CEST
Purpose: plot data as map from netcdf
         example script
Usage: python plot_map_from_netcdf.py filename varname
              [-id inputdir -od outputdir -r region -pt plottype -p plotname
               -t title -cm colormap -l levels -a aggregation]
'''
# Load modules for this script
import argparse
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from scipy import signal
import os
from os.path import expanduser
home = expanduser("~") # Get users home directory
import sys
sys.path.insert(1, '/home/rlorenz/scripts/plot_scripts/utils/')
import argparse
import math
import matplotlib.pyplot as plt
from draw_utils import draw

###
# How many and which arguments were passed from command line?
# first argument [0] is always script name
###
parser = argparse.ArgumentParser()
parser.add_argument("filename", help = "the filename of the netcdf to plot")
parser.add_argument("varname", help = "the variable name to read from the file")
parser.add_argument("-id", "--indir", help = "input directory, " + 
                    "default is current")
parser.add_argument("-od", "--outdir", help = "output directory, " +
                    "if not given defaults to indir")
parser.add_argument("-r", "--region", help = "specify region for " +
                    "non-global plots")
parser.add_argument("-pt", "--plttype", help = "specify output file type, " + 
                    "default is pdf")
parser.add_argument("-p", "--pltname", help = "specify plotname, " + 
                    "default is same as filename")
parser.add_argument("-t", "--title", help = "specify alternative title for" +   
                    " plot, needs to be put into quotes if contains any " +
                    "spaces, default is longname [units] (if specified in " + 
                    "netcdf)")
parser.add_argument("-cm", "--cmap", help = "specify colormap, default is " +
                    "'YlRdBu'")
parser.add_argument("-l", "--lev", nargs = 3, type = float,
                    help = "specify levels of contours, " +
                    "uses np.arange(start, end, delta), so needs 3 numbers, " +
                    "default is specified by min and max of data")
parser.add_argument("-a", "--agg", help = "how to aggregate variable if " +
                    "first dimension larger than 1 (assumed to be time), " + 
                    "default is 'mean', " +
                    "other options are 'std', 'min', 'max'")
args = parser.parse_args()

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

###
# input needs to be passed as arguments from command line
###
filename = args.filename
varname = args.varname

###
# optional input arguments
#####

if args.indir:
    indir = args.indir
else:
    print "No input directory specified, looking in current:"
    indir = os.path.dirname(os.path.abspath(__file__))
    print indir

if args.region:
    region = args.region # needed for drawing routine if data other than global
else:
    region = "GLOBAL"

# define format of figures
if args.plttype:
    plottype = args.plttype
else:
    plottype = ".pdf"

if args.pltname:
    plotname = args.pltname
else:
    plotname = os.path.splitext(filename)[0]

if args.title:
    title = args.title

if args.cmap:
    colormap = args.cmap

# range for plotting,
# if not defined will be defined based on min and max of data
if args.lev:
    lev = np.arange(args.lev[0], args.lev[1] + args.lev[2], args.lev[2])

# define output directory, will be created if does not exist yet
# if not defined plot will be put into users home
if args.outdir:
    outdir = args.outdir
else:
    outdir = indir
if (os.access(outdir, os.F_OK) == False):
    os.makedirs(outdir)

outfile = '%s/%s_%s%s' %(outdir, plotname, region, plottype)

###
# Read data
###
path = "%s/%s" %(indir, filename)

ifile = nc.Dataset(path)
var = ifile.variables[varname][:]

# try to read units and long_name attributes for default title
try:
    title
except NameError:
    try:
        units = ifile.variables[varname].units
    except AttributeError:
        print('Warning: no unit defined for variable, ' +
              'default title will not work')
    else:
        if (units == 'Celsius') or (units == 'degC'):
            degree_sign = u'\N{DEGREE SIGN}'
            units = degree_sign + "C"
    try:
        long_name = ifile.variables[varname].long_name
    except AttributeError:
        print('Warning: no longname defined for variable, ' +
              'default title will not work')

# read latitudes, either named 'latitude', or 'lat'
try:
    lat = ifile.variables['latitude'][:]
except KeyError:
    lat = ifile.variables['lat'][:]
# read longitudes, either named 'longitude', or 'lon'
try:
    lon = ifile.variables['longitude'][:]
except KeyError:
    lon = ifile.variables['lon'][:]

# close file
ifile.close()

###
# plot data
###
# check dimension of var, if time dimension larger than 1 aggregate
ndim = len(var.shape)
if ndim == 2:
    data = var
    if args.agg:
        print 'Warning: args.agg not used, variable has dimension %s' %(
            str(var.shape))
elif ndim > 2:
    if var.shape[0] == 1:
        data = np.squeeze(var)
        if args.agg:
            print 'Warning: args.agg not used, variable has dimension %s' %(
                str(var.shape))
    elif var.shape[1] == 1 or var.shape[2] == 1:
        print 'Error: variable has dimension %s, cannot plot map.' %(
            str(var.shape))
        sys.exit()
    elif var.shape[0] > 1:
        if args.agg:
            if args.agg == 'mean':
                data = np.nanmean(var, 0)
            elif args.agg == 'std':
                if np.isnan(var).any():
                    var_dtr = np.empty((var.shape))
                    for ilat in xrange(len(lat)):
                        for ilon in xrange(len(lon)):
                            tmp = var[:, ilat, ilon]
                            if np.isnan(tmp).any():
                                var_dtr[:, ilat, ilon] = np.NaN
                            else:
                                var_dtr[:, ilat, ilon] = signal.detrend(
                                    tmp, axis = 0, type = 'linear', bp = 0)
                else:
                    var_dtr = signal.detrend(var, axis = 0)
                data = np.nanstd(np.squeeze(var), axis = 0)
            elif args.agg == 'min':
                data = np.nanmin(var, 0)
            elif args.agg == 'max':
                data = np.nanmax(var, 0)
            else:
                print("Error: invalid agg argument, either 'mean', 'std',"
                "'min', or 'max'")
                sys.exit()
        else:
            data = np.nanmean(var, 0)
else:
    print 'Error: variable has dimension %s, cannot plot map.' %(str(var.shape))
    sys.exit()

try:
    title
except NameError:
    try:
        long_name
        units
    except NameError:
        title = None
    else:
        title = '%s [%s]' %(long_name, units)

try:
    lev
except NameError:
    var_min = math.ceil(np.min(var))
    var_max = math.floor(np.max(var))
    delta = abs(round((var_max - var_min) / 7.0))
    start = var_min
    end = var_min + 7.0 * delta
    lev = np.arange(start, end, delta)
#lev = np.arange(-6, 6, 1)
try:
    colormap
except NameError:
    draw(data, lat, lon, title = title, region = region,
         levels = lev)
else:
    draw(data, lat, lon, title = title, region = region,
         colors = colormap, levels = lev)
plt.savefig(outfile)
