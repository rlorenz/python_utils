#!/usr/bin/python
'''
File Name : calc_trend_TLL.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-04-2016
Modified: Mon 18 Apr 2016 01:48:52 PM CEST
Purpose: calculate trend over time at every grid point,
         dimension of x must be time, lat,lon


'''
import numpy as np
from scipy import stats

def trend_TLL(x,drop=True):
    if isinstance(x, np.ma.core.MaskedArray):
        x = x.filled(np.NaN)
    dims = x.shape
    if drop==False:
            trend = np.empty((1,dims[1],dims[2]))
    else:
            trend = np.empty((dims[1],dims[2]))
    for lat in range(dims[1]):
        for lon in range(dims[2]):
                if drop==False:
                        trend[0,lat,lon] = stats.linregress(range(dims[0]),x[:,lat,lon]).slope
                else:
                        trend[lat,lon] = stats.linregress(range(dims[0]),x[:,lat,lon]).slope
    return trend
