'''
File Name: boxplot_custom.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 07-12-2017
Modified: Thu 07 Dec 2017 03:54:31 PM CET
Purpose: routine for drawing boxplot based on pre-calculated
         mean, 5th, 25th, 75th, 95th percentiles
'''
import numpy as np
import matplotlib as mpl
import matplotlib.pylab as plt

def boxplot(df, ax = None, box_width = 0.2, whisker_size = 20, mean_size = 10,
            median_size = 10 , line_width = 1.5, xoffset = 0, color = 0):
    """Plots a boxplot from existing percentiles.

    Parameters
    ----------
    df: pandas DataFrame
    ax: pandas AxesSubplot
        if to plot on en existing axes
    box_width: float
    whisker_size: float
        size of the bar at the end of each whisker
    mean_size: float
        size of the mean symbol
    color: int or rgb(list)
        If int particular color of property cycler is taken.
        Example of rgb: [1,0,0] (red)

    Returns
    -------
    f, a, boxes, vlines, whisker_tips, mean, median
    """

    if type(color) == int:
        color = mpl.rcParams['axes.prop_cycle'].by_key()['color'][color]

    if ax:
        a = ax
        f = a.get_figure()
    else:
        f, a = plt.subplots()

    boxes = []
    vlines = []
    xn = []
    for row in df.iterrows():
        x = row[0] + xoffset
        xn.append(x)

        # box
        y = row[1][25]
        height = row[1][75] - row[1][25]
        box = plt.Rectangle((x - box_width / 2, y), box_width, height)
        a.add_patch(box)
        boxes.append(box)

        # whiskers
        y = (row[1][95] + row[1][5]) / 2
        vl = a.vlines(x, row[1][5], row[1][95])
        vlines.append(vl)

    for b in boxes:
        b.set_linewidth(line_width)
        b.set_facecolor([1, 1, 1, 1])
        b.set_edgecolor(color)
        b.set_zorder(2)

    for vl in vlines:
        vl.set_color(color)
        vl.set_linewidth(line_width)
        vl.set_zorder(1)

    whisker_tips = []
    if whisker_size:
        g, = a.plot(xn, df[5], ls = '')
        whisker_tips.append(g)

        g, = a.plot(xn, df[95], ls = '')
        whisker_tips.append(g)

    for wt in whisker_tips:
        wt.set_markeredgewidth(line_width)
        wt.set_color(color)
        wt.set_markersize(whisker_size)
        wt.set_marker('_')

    mean = None
    if mean_size:
        g, = a.plot(xn, df['mean'], ls = '')
        g.set_marker('o')
        g.set_markersize(mean_size)
        g.set_zorder(20)
        g.set_markerfacecolor('None')
        g.set_markeredgewidth(line_width)
        g.set_markeredgecolor(color)
        mean = g

    median = None
    if median_size:
        g, = a.plot(xn, df['median'], ls = '')
        g.set_marker('_')
        g.set_markersize(median_size)
        g.set_zorder(20)
        g.set_markeredgewidth(line_width)
        g.set_markeredgecolor(color)
        median = g

    a.set_ylim(np.nanmin(df) - 1, np.nanmax(df) + 1)
    return f, a, boxes, vlines, whisker_tips, mean, median

# function for setting the colors of the box plots
def setBoxColors(bp, colors = None):
    if colors == 'GrOrBlPuGy':
        plt.setp(bp['boxes'][0], color = 'green')
        plt.setp(bp['caps'][0], color = 'green')
        plt.setp(bp['caps'][1], color = 'green')
        plt.setp(bp['whiskers'][0], color = 'green')
        plt.setp(bp['whiskers'][1], color = 'green')
        plt.setp(bp['fliers'][0], color = 'green', markeredgecolor = 'green')
        plt.setp(bp['medians'][0], color = 'green')
        plt.setp(bp['means'][0], markerfacecolor = 'green',
                 markeredgecolor = 'green')

        plt.setp(bp['boxes'][1], color = 'darkorange')
        plt.setp(bp['caps'][2], color = 'darkorange')
        plt.setp(bp['caps'][3], color = 'darkorange')
        plt.setp(bp['whiskers'][2], color = 'darkorange')
        plt.setp(bp['whiskers'][3], color = 'darkorange')
        plt.setp(bp['fliers'][1], color = 'darkorange',
                 markeredgecolor = 'darkorange')
        plt.setp(bp['medians'][1], color = 'darkorange')
        plt.setp(bp['means'][1], markerfacecolor = 'darkorange',
                 markeredgecolor = 'darkorange')

        plt.setp(bp['boxes'][2], color = 'royalblue')
        plt.setp(bp['caps'][4], color = 'royalblue')
        plt.setp(bp['caps'][5], color = 'royalblue')
        plt.setp(bp['whiskers'][4], color = 'royalblue')
        plt.setp(bp['whiskers'][5], color = 'royalblue')
        plt.setp(bp['fliers'][2], color = 'royalblue',
                 markeredgecolor = 'royalblue')
        plt.setp(bp['medians'][2], color = 'royalblue')
        plt.setp(bp['means'][2], markerfacecolor = 'royalblue',
                 markeredgecolor = 'royalblue')

        plt.setp(bp['boxes'][3], color = 'mediumorchid')
        plt.setp(bp['caps'][6], color = 'mediumorchid')
        plt.setp(bp['caps'][7], color = 'mediumorchid')
        plt.setp(bp['whiskers'][6], color = 'mediumorchid')
        plt.setp(bp['whiskers'][7], color = 'mediumorchid')
        plt.setp(bp['fliers'][3], color = 'mediumorchid',
                 markeredgecolor = 'mediumorchid')
        plt.setp(bp['medians'][3], color = 'mediumorchid')
        plt.setp(bp['means'][3], markerfacecolor = 'mediumorchid',
                 markeredgecolor = 'mediumorchid')

        plt.setp(bp['boxes'][4], color = 'grey')
        plt.setp(bp['caps'][8], color = 'grey')
        plt.setp(bp['caps'][9], color = 'grey')
        plt.setp(bp['whiskers'][8], color = 'grey')
        plt.setp(bp['whiskers'][9], color = 'grey')
        plt.setp(bp['fliers'][4], color = 'grey', markeredgecolor = 'grey')
        plt.setp(bp['medians'][4], color = 'grey')
        plt.setp(bp['means'][4], markerfacecolor = 'grey',
                 markeredgecolor = 'grey')
    else:
        plt.setp(bp['boxes'][0], color = 'k')
        plt.setp(bp['caps'][0], color = 'k')
        plt.setp(bp['caps'][1], color = 'k')
        plt.setp(bp['whiskers'][0], color = 'k')
        plt.setp(bp['whiskers'][1], color = 'k')
        plt.setp(bp['fliers'][0], color = 'k', markeredgecolor = 'k')
        plt.setp(bp['medians'][0], color = 'k')
        plt.setp(bp['means'][0], markerfacecolor = 'k', markeredgecolor = 'k')

        plt.setp(bp['boxes'][1], color = 'blue')
        plt.setp(bp['caps'][2], color = 'blue')
        plt.setp(bp['caps'][3], color = 'blue')
        plt.setp(bp['whiskers'][2], color = 'blue')
        plt.setp(bp['whiskers'][3], color = 'blue')
        plt.setp(bp['fliers'][1], color = 'blue', markeredgecolor = 'blue')
        plt.setp(bp['medians'][1], color = 'blue')
        plt.setp(bp['means'][1], markerfacecolor = 'blue',
                 markeredgecolor = 'blue')

        plt.setp(bp['boxes'][2], color = 'green')
        plt.setp(bp['caps'][4], color = 'green')
        plt.setp(bp['caps'][5], color = 'green')
        plt.setp(bp['whiskers'][4], color = 'green')
        plt.setp(bp['whiskers'][5], color = 'green')
        plt.setp(bp['fliers'][2], color = 'green', markeredgecolor = 'green')
        plt.setp(bp['medians'][2], color = 'green')
        plt.setp(bp['means'][2], markerfacecolor = 'green',
                 markeredgecolor = 'green')

        plt.setp(bp['boxes'][3], color = 'm')
        plt.setp(bp['caps'][6], color = 'm')
        plt.setp(bp['caps'][7], color = 'm')
        plt.setp(bp['whiskers'][6], color = 'm')
        plt.setp(bp['whiskers'][7], color = 'm')
        plt.setp(bp['fliers'][3], color = 'm', markeredgecolor = 'm')
        plt.setp(bp['medians'][3], color = 'm')
        plt.setp(bp['means'][3], markerfacecolor = 'm', markeredgecolor = 'm')
