"""
Some utilities for plotting maps
"""

import os
import pdb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mc
from mpl_toolkits.basemap import Basemap, addcyclic, maskoceans, shiftgrid

## function that draws map
def draw(data, lats, lons, title=None, subpanel=None, method='contourf', vmin=None, vmax=None, colors='YlOrRd', levels=15 ,sig = None, masko = None, region = None):
     fig = plt.figure(figsize=(8,6))

     ax = fig.add_axes([0.1,0.1,0.8,0.8])

     max_lat = np.amax(lats)
     min_lat = np.amin(lats)
     max_lon = np.amax(lons)
     min_lon = np.amin(lons)

     if region:
               if region == 'ARC':
                    ## for arctic region draw polar stereographic projection
                    m = Basemap(projection='npstere', boundinglat=70, lon_0=270, resolution='c')
               else:
                    m = Basemap(projection = 'cyl', llcrnrlat = min_lat, urcrnrlat = max_lat, llcrnrlon = min_lon, urcrnrlon = max_lon, resolution = 'c')
               m.drawcoastlines()
               m.drawparallels(np.arange(-90., 91., 10.), labels=[1, 0, 0, 0])
               m.drawmeridians(np.arange(-180., 181., 20.), labels=[0, 0, 0, 1])
     else:
          m = Basemap(projection = 'cyl', llcrnrlat = min_lat, urcrnrlat = max_lat, llcrnrlon = - 180, urcrnrlon = 180, resolution = 'c')
          m.drawcoastlines()
          m.drawparallels(np.arange(- 90., 91., 30.), labels = [1, 0, 0, 0])
          m.drawmeridians(np.arange(- 180., 181., 60.), labels = [0, 0, 0, 1])

     if region is None:
          if (min_lon >= 0) & (max_lon > 180):
               lons_orig = lons[:]
               data, lons =  shiftgrid(180., data, lons, start = False)

     data, lonsnew = addcyclic(data, lons)
     lons2d, lats2d = np.meshgrid(lonsnew, lats)
     x, y = m(lons2d, lats2d)

     if masko:
	data = maskoceans(lons2d, lats2d, data, resolution = 'h', grid = 1.25, inlands = True)

     cmap = plt.get_cmap(colors)
     if (type(levels) != int):
          diff = (levels[0] - levels[1]) - (levels[1] - levels[2])
     else:
          diff = 0
     if isinstance(levels, int) | (diff == 0):
          cs = m.contourf(x, y, data, levels, cmap = cmap, extend = 'both')
     else:
          norm = mc.BoundaryNorm(levels, cmap.N)
          cs = m.contourf(x, y, data, levels, cmap = cmap, norm = norm, extend = 'both')
     cs.set_clim(levels[0], levels[-1])
     if sig is not None:
	if (min_lon >= 0) & (max_lon > 180):
        	sig, lons =  shiftgrid(180., sig, lons_orig, start = False)
	sig, lonsnew = addcyclic(sig, lons)
	if masko:
		sig = maskoceans(lons2d, lats2d, sig, resolution = 'h', grid = 1.25, inlands = True)
     	hat = m.contourf(x, y, sig, [ - 0.5, 0.5, 2], hatches = [None, 'xxx'], alpha = 0.0)
        #hat = m.contourf(x, y, sig, [ - 0.5, 0.5, 2], hatches = [None, '///'], alpha=0.0)

     cbar = m.colorbar(cs, location = 'bottom', pad = "10%", format='%.2f')
     if vmin is not None:
         plt.clim(vmin = vmin, vmax = vmax)
     if title: 
          plt.title(title)
     if subpanel:
	  plt.text( - 180, 82.5, subpanel)
     return m

#function that draws rectangle over map e.g. to indicate region
def plot_rectangle(bmap, lonmin, lonmax, latmin, latmax):
    xs = [lonmin, lonmax, lonmax, lonmin, lonmin]
    ys = [latmin, latmin, latmax, latmax, latmin]
    bmap.plot(xs, ys, latlon = True, color = 'black')

#function that draws map and rectangle over regions
def draw_reg(data, lats, lons, title = None, subpanel = None, method = 'contourf', vmin = None, vmax = None, colors = 'YlOrRd', levels = 15, sig = None, masko = None, name=['AU'], lonmin=[110], lonmax=[160], latmin=[-45], latmax=[-10]):
     fig = plt.figure(figsize=(8, 6))

     ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])

     max_lat = np.amax(lats)
     min_lat = np.amin(lats)
     max_lon = np.amax(lons)
     min_lon = np.amin(lons)

     m = Basemap(projection = 'cyl', llcrnrlat = min_lat, urcrnrlat = max_lat, llcrnrlon = -180, urcrnrlon = 180, resolution = 'c')
     m.drawcoastlines()
     m.drawparallels(np.arange( - 90., 91., 30.), labels = [1, 0, 0, 0])
     m.drawmeridians(np.arange( - 180., 181., 60.), labels = [0, 0, 0, 1])

     if (min_lon >= 0) & (max_lon > 180):
        lons_orig = lons[:]
        data, lons =  shiftgrid(180., data, lons, start = False)

     data, lonsnew = addcyclic(data, lons)
     lons2d, lats2d = np.meshgrid(lonsnew, lats)
     x, y = m(lons2d, lats2d)

     if masko:
	data = maskoceans(lons2d, lats2d, data, resolution = 'h', grid = 1.25, inlands = True)

     cs = m.contourf(x, y, data, levels, cmap=plt.get_cmap(colors), extend = 'both')
     if sig is not None:
     	if (min_lon >= 0) & (max_lon > 180):
        	sig, lons =  shiftgrid(180., sig, lons_orig, start = False)
	sig, lonsnew = addcyclic(sig, lons)
	if masko:
		sig = maskoceans(lons2d, lats2d, sig, resolution = 'h', grid = 1.25, inlands = True)
     	hat = m.contourf(x, y, sig, [ - 0.5, 0.5, 2], hatches = [None, 'xx'], alpha = 0.0)

     for r in range(len(name)):
          plot_rectangle(m, lonmin[r], lonmax[r], latmin[r], latmax[r])
          plt.text(lonmin[r] - 2, latmin[r], name[r], fontsize = 10, fontweight = 'bold',
                    ha = 'right', va = 'bottom', color = 'k')

     cbar = m.colorbar(cs, location = 'bottom', pad = "10%")
     plt.clim(vmin = vmin, vmax = vmax)
     if title: 
          plt.title(title)
     if subpanel:
          plt.text( - 180, 82.5, subpanel)
     return m
