#!/usr/bin/python
'''
File Name : func_read_data.py
Author: Ruth Lorenz (ruth.lorenz@env.ethz.ch)
Created: 10-03-2016
Modified: Tue 18 Oct 2016 09:24:40 AM CEST
Purpose: read data from netcdfs given time period and variable
'''
import netCDF4 as nc # to work with NetCDF files
import numpy as np
from netcdftime import utime
import datetime as dt
# for py3 forward compatibility
from builtins import range

def func_read_netcdf(infile, variable, lonvar = 'lon', latvar = 'lat', timevar = 'time', time_cal = None, var_units = None, syear = None, eyear = None):
    ifile = nc.Dataset(infile, mode = 'r')
    lon = ifile.variables[lonvar][:]
    lat = ifile.variables[latvar][:]
    time = ifile.variables[timevar]
    if var_units == True:
        #data = ifile.variables[variable]
        unit = ifile.variables[variable].units
        if syear is not None:
            #Create dates from netcdf time to read variable over given time period
            time_units = time.units
            if time_cal is None:
                try:
                    time_cal = time.calendar
                except AttributeError:
                    time_cal = "standard"
            cdftime = utime(time_units, calendar = time_cal)
            dates = cdftime.num2date(time[:])
            years = np.asarray([dates[i].year for i in range(len(dates))])
            dates = dates[(years >= syear) & (years <= eyear)]
            time = time[(years >= syear) & (years <= eyear)]
            data = ifile.variables[variable][(years >= syear) &
                                             (years <= eyear), :, :]
    else:
        time_units = time.units
        try:
            time_cal = time.calendar
        except AttributeError:
            time_cal = "standard"
            cdftime = utime(time_units, calendar = time_cal)
            dates = cdftime.num2date(time[:])
            data = ifile.variables[variable][:]
            time = time[:]
    ifile.close()
    if var_units == True:
        return {'data':data, 'unit':unit, 'lat':lat, 'lon':lon, 'time':time,
                'tunit':time_units, 'tcal':time_cal, 'dates':dates}
    else:
        return {'data':data, 'lat':lat, 'lon':lon, 'time':time,
                'tunit':time_units, 'tcal':time_cal, 'dates':dates}
